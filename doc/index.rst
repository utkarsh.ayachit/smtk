##################################
SMTK: Simulation Modeling Tool Kit
Version 1.0.0
##################################

.. highlight:: c++

.. role:: cxx(code)
   :language: c++

.. findimage:: smtk-logo.*

Contents:

.. toctree::
   :maxdepth: 4

   userguide/index.rst
   tutorials/index.rst


##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
